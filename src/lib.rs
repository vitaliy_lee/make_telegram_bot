use std::error;
use std::io;
use std::io::Write;
use std::process;

#[macro_use]
extern crate frunk;

use teloxide::prelude::*;

use once_cell::sync::OnceCell;

pub mod config;
pub mod dialogue;

use crate::dialogue::Dialogue;

pub static CONFIG: OnceCell<config::AppConfig> = OnceCell::new();

pub type Result<T> = std::result::Result<T, Box<dyn error::Error>>;

pub async fn run_make_target(
    cx: TransitionIn<AutoSend<Bot>>,
    directory: &str,
    target: &str,
) -> TransitionOut<Dialogue> {
    let argument = format!("make {}", target);

    let answer = format!("Running {}...", argument);
    cx.answer(answer).await?;

    let output = process::Command::new("sh")
        .arg("-c")
        .current_dir(directory)
        .arg(argument)
        .output()
        .expect("failed to execute process");

    log::debug!("status: {}", output.status);

    io::stdout().write_all(&output.stdout).unwrap();
    io::stderr().write_all(&output.stderr).unwrap();

    if output.status.success() {
        cx.answer("✅ PASSED").await?;
    } else {
        let stdout = String::from_utf8(output.stdout).unwrap();
        let stderr = String::from_utf8(output.stderr).unwrap();

        cx.answer(format!("❌ FAILED:\n{}\n{}", stderr, stdout))
            .await?;
    }

    exit()
}
