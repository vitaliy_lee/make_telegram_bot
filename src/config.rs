use std::collections::HashMap;
use std::fs;
use std::io::prelude::*;
use std::path::Path;

use once_cell::sync::OnceCell;

use toml;

use serde::Deserialize;

static INSTANCE: OnceCell<AppConfig> = OnceCell::new();

#[derive(Debug, Clone, Deserialize)]
pub struct AppConfig {
    pub bot: BotConfig,
    pub projects: HashMap<String, String>,
}

impl AppConfig {
    fn read<P: AsRef<Path>>(path: P) -> super::Result<AppConfig> {
        let path = path.as_ref();

        let mut config_file = fs::File::open(path)?;

        let mut content = String::new();
        config_file.read_to_string(&mut content)?;

        let app_confg: AppConfig = toml::from_str(&content)?;
        Ok(app_confg)
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct BotConfig {
    pub token: String,
    pub url: String,
    pub key: String,
    pub certificate: String,
    pub listen_address: String,
    pub set_webhook: bool,
}

pub fn read<P: AsRef<Path>>(path: P) {
    let config = AppConfig::read(path).expect("Parse config");

    INSTANCE.set(config).unwrap();
}

pub fn get() -> &'static AppConfig {
    INSTANCE.get().expect("Get config instance")
}
