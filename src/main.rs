use std::convert::Infallible;
use std::env;
use std::net::SocketAddr;
use std::path::Path;
use std::process;

use tokio::sync::mpsc;
use tokio_stream::wrappers::UnboundedReceiverStream;

use reqwest::{StatusCode, Url};

use warp::Filter;

use teloxide::dispatching::stop_token::AsyncStopToken;
use teloxide::dispatching::update_listeners::{self, StatefulListener};
use teloxide::prelude::*;
use teloxide::types::{InputFile, Update};

use make_telegram_bot::config;
use make_telegram_bot::dialogue::Dialogue;

async fn handle_message(
    cx: UpdateWithCx<AutoSend<Bot>, Message>,
    dialogue: Dialogue,
) -> TransitionOut<Dialogue> {
    match cx.update.text().map(ToOwned::to_owned) {
        None => {
            cx.answer("Send me a text message").await?;
            next(dialogue)
        }
        Some(answer) => dialogue.react(cx, answer).await,
    }
}

async fn handle_rejection(error: warp::Rejection) -> Result<impl warp::Reply, Infallible> {
    log::error!("Cannot process the request due to: {:?}", error);
    Ok(StatusCode::INTERNAL_SERVER_ERROR)
}

pub async fn webhook<'a>(
    bot: AutoSend<Bot>,
    config: &config::BotConfig,
) -> impl update_listeners::UpdateListener<Infallible> {
    if config.set_webhook {
        let url = Url::parse(&config.url).expect("Parse webhook URL");

        bot.set_webhook(url)
            .certificate(InputFile::file(&config.certificate))
            .send()
            .await
            .expect("Setup a webhook");
    }

    let (tx, rx) = mpsc::unbounded_channel();

    let server = warp::post()
        .and(warp::body::json())
        .map(move |json: serde_json::Value| {
            if let Ok(update) = Update::try_parse(&json) {
                tx.send(Ok(update))
                    .expect("Cannot send an incoming update from the webhook")
            }

            StatusCode::OK
        })
        .recover(handle_rejection);

    let tls_server = warp::serve(server)
        .tls()
        .key_path(&config.key)
        .cert_path(&config.certificate);

    let address = config
        .listen_address
        .parse::<SocketAddr>()
        .expect("parse listen address");

    let (stop_token, stop_flag) = AsyncStopToken::new_pair();

    let (_address, task) = tls_server.bind_with_graceful_shutdown(address, stop_flag);

    tokio::spawn(task);

    let stream = UnboundedReceiverStream::new(rx);

    fn stream_fn<S, T>(state: &mut (S, T)) -> &mut S {
        &mut state.0
    }

    StatefulListener::new(
        (stream, stop_token),
        stream_fn,
        |state: &mut (_, AsyncStopToken)| state.1.clone(),
    )
}

async fn run() {
    teloxide::enable_logging!();

    log::info!("Starting make_bot...");

    let token = config::get().bot.token.clone();

    let bot = Bot::new(token).auto_send();
    let cloned_bot = bot.clone();

    let bot_config = &config::get().bot;

    teloxide::dialogues_repl_with_listener(
        bot,
        |message, dialogue| async move {
            handle_message(message, dialogue)
                .await
                .expect("Handle message")
        },
        webhook(cloned_bot, bot_config).await,
    )
    .await;
}

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("usage: make_bot <config_path>");
        process::exit(1);
    }

    let config_path = Path::new(&args[1]);
    config::read(config_path);

    run().await;
}
