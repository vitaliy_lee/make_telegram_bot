use teloxide::prelude::*;

use crate::dialogue::Dialogue;
use crate::run_make_target;

#[derive(Clone, Generic)]
pub struct ReceiveTargetNameState {
    pub project_directory: String,
}

#[teloxide(subtransition)]
async fn receive_target_name(
    state: ReceiveTargetNameState,
    cx: TransitionIn<AutoSend<Bot>>,
    answer: String,
) -> TransitionOut<Dialogue> {
    run_make_target(cx, &state.project_directory, &answer).await
}
