use teloxide::prelude::*;

use crate::dialogue::{states::ReceiveProjectNameState, Dialogue};

#[derive(Clone)]
pub struct StartState;

#[teloxide(subtransition)]
async fn start(
    _state: StartState,
    cx: TransitionIn<AutoSend<Bot>>,
    _answer: String,
) -> TransitionOut<Dialogue> {
    cx.answer("Enter project name").await?;
    next(ReceiveProjectNameState)
}
