use teloxide::prelude::*;

use crate::config;
use crate::dialogue::{states::receive_target_name::ReceiveTargetNameState, Dialogue};

#[derive(Clone, Generic)]
pub struct ReceiveProjectNameState;

#[teloxide(subtransition)]
async fn receive_project_name(
    state: ReceiveProjectNameState,
    cx: TransitionIn<AutoSend<Bot>>,
    answer: String,
) -> TransitionOut<Dialogue> {
    let config = config::get();

    match config.projects.get(&answer) {
        Some(directory) => {
            cx.answer("Enter target name").await?;
            next(ReceiveTargetNameState::up(state, directory.clone()))
        }
        None => {
            cx.answer("Project not found").await?;
            exit()
        }
    }
}
