mod states;

use teloxide::macros::Transition;

use derive_more::From;

use crate::dialogue::states::{ReceiveProjectNameState, ReceiveTargetNameState, StartState};

#[derive(Transition, Clone, From)]
pub enum Dialogue {
    Start(StartState),
    ReceiveProjectName(ReceiveProjectNameState),
    ReceiveTargetName(ReceiveTargetNameState),
}

impl Default for Dialogue {
    fn default() -> Self {
        Self::Start(StartState)
    }
}
