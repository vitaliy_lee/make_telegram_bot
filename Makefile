build:
	@cargo build

clean:
	@cago clean

release:
	@cargo build --release

install:
	@cp target/release/make_telegram_bot /opt/make_telegram_bot/bin/

test:
	@cargo test

start:
	@systemctl start make-telegram-bot.service

stop:
	@systemctl stop make-telegram-bot.service

status:
	systemctl status make-telegram-bot.service
